#ifndef _LIBLANGTAG_LIBLANGTAG_LT_STDINT_H
#define _LIBLANGTAG_LIBLANGTAG_LT_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "liblangtag 0.6.7"
/* generated using gnu compiler gcc (GCC) 13.2.1 20231110 (Red Hat 13.2.1-5) */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
